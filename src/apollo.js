import ApolloClient from "apollo-boost";

import { authService } from "./auth";

// Client State
const clientState = {
  defaults: {
    navbar: "MAIN",
    isActionGridOpen: false,
    isDeleteNodeDialogOpen: false,
    loggedIn: true,
  },

  resolvers: {},

  typeDefs: `
    enum Navbar {
      MAIN
      SEARCH
    }

    type Query {
      navbar: Navbar
      isActionGridOpen: Boolean!
      isDeleteNodeDialogOpen: Boolean!
      loggedIn: Boolean!
    }
  `,
};

const request = async (operation) => {
  const token = authService.selectors.token();
  operation.setContext({
    headers: {
      Authorization: token ? `Bearer ${token}` : "",
    },
  });
};

export const createClient = () => {
  const client = new ApolloClient({
    uri: window["CONCEPTS_CONFIG"].REACT_APP_GRAPHQL_ENDPOINT || "",
    headers: {
      // "x-hasura-admin-secret": "",
    },
    request,
    clientState,
  });

  return client;
};
