import React from "react";
import gql from "graphql-tag";
import { useApolloClient, useQuery } from "@apollo/react-hooks";
import { NavLink as RRNavLink } from "react-router-dom";
import { useLocation } from "react-router-dom";
import { Navbar as BsNavbar, NavbarBrand, Nav, NavItem } from "reactstrap";
import { NavLink, Form, Input, Button } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch, faUser } from "@fortawesome/free-solid-svg-icons";
import { faArrowLeft, faSignOutAlt } from "@fortawesome/free-solid-svg-icons";
import { contains } from "ramda";
import { useAuth } from "../../auth";
import { ROUTES } from "../../Router";
import { useSearch } from "../search/Provider";

const changeNavbar = (client, navbar) => client.writeData({ data: { navbar } });

const Navbar = () => {
  const client = useApolloClient();
  return (
    <BsNavbar light className="bg-light py-2">
      <NavbarBrand tag={RRNavLink} to={ROUTES.HOME}>
        Nodes
      </NavbarBrand>
      <Nav className="flex-row" navbar>
        <NavItem>
          <NavLink
            className="px-3 clickable"
            onClick={() => changeNavbar(client, "SEARCH")}
          >
            <FontAwesomeIcon icon={faSearch} size="lg" />
          </NavLink>
        </NavItem>
        <LoginNavItem client={client} />
        <NavItem />
      </Nav>
    </BsNavbar>
  );
};

const LoginNavItem = () => {
  const { pathname } = useLocation();
  const { isAuthenticated, logout } = useAuth();

  return isAuthenticated ? (
    <NavLink className="px-3 clickable" onClick={() => logout()}>
      <FontAwesomeIcon icon={faSignOutAlt} size="lg" />
    </NavLink>
  ) : (
    <NavLink
      className="px-3"
      tag={RRNavLink}
      to={ROUTES.LOGIN}
      isActive={() => contains(pathname, [ROUTES.REGISTER, ROUTES.LOGIN])}
    >
      <FontAwesomeIcon icon={faUser} size="lg" />
    </NavLink>
  );
};

const SearchNavbar = () => {
  const { setFilter } = useSearch();
  const client = useApolloClient();
  return (
    <BsNavbar light className="bg-light flex-nowrap py-2">
      <Nav navbar className="pr-2">
        <NavItem>
          <NavLink
            className="clickable"
            onClick={() => changeNavbar(client, "MAIN")}
          >
            <FontAwesomeIcon icon={faArrowLeft} size="lg" />
          </NavLink>
        </NavItem>
      </Nav>
      <Form
        inline
        className="flex-nowrap"
        onSubmit={(e) => {
          e.preventDefault();
          e.target.query.blur();
          const query = e.target.query.value;
          setFilter("query", `%${query}%`);
        }}
      >
        <Input
          type="search"
          name="query"
          placeholder="Search"
          autoComplete="off"
          className="mx-3"
        />
        <Button color="link" className="clickable text-secondary px-1">
          <FontAwesomeIcon icon={faSearch} size="lg" />
        </Button>
      </Form>
    </BsNavbar>
  );
};

const GET_NAVBAR = gql`
  {
    navbar @client
  }
`;

const NavbarRouter = () => {
  const { data } = useQuery(GET_NAVBAR);
  if (!data) return <Navbar />;
  const { navbar } = data;
  return navbar === "MAIN" ? <Navbar /> : <SearchNavbar />;
};

export default NavbarRouter;
