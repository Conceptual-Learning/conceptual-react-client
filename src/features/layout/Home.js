import React from "react";
import { useAuth } from "../../auth";

const Home = () => {
  const { user } = useAuth();
  return (
    <h3 className="text-center">
      Hello {user ? user.email : "Anonymous"}, Welcome Home
    </h3>
  );
};

export default Home;
