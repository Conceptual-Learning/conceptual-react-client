import React, { useState } from "react";
import * as R from "ramda";
import { Button, ButtonGroup, FormGroup, Input } from "reactstrap";
import { Container, Row, Col } from "reactstrap";
import { Route, Link, useLocation, useHistory } from "react-router-dom";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";
import {
  FacebookLoginButton,
  GoogleLoginButton,
  createButton,
} from "react-social-login-buttons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";
import QRCode from "qrcode.react";
import { useAuth } from "../../auth";

import { ROUTES } from "../../Router";

export default () => {
  return (
    <Container className="h-75">
      <br />
      <Row className="align-items-center justify-content-center h-100">
        <Col md="4" style={{ height: "270px" }}>
          <Route path={[ROUTES.REGISTER, ROUTES.LOGIN]}>
            <LoginNavs />
          </Route>
          <LoginForms />
        </Col>
      </Row>
    </Container>
  );
};

const LoginNavs = () => {
  const history = useHistory();
  const { pathname } = useLocation();
  return (
    <Row>
      <Col>
        <FormGroup>
          <ButtonGroup className="w-100">
            <Button
              size="lg"
              outline={pathname !== ROUTES.LOGIN}
              className="w-100 clickable"
              color="primary"
              onClick={() => history.replace(ROUTES.LOGIN)}
            >
              Login
            </Button>
            <Button
              size="lg"
              outline={pathname !== ROUTES.REGISTER}
              className="w-100 clickable"
              color="primary"
              onClick={() => history.replace(ROUTES.REGISTER)}
            >
              Register
            </Button>
          </ButtonGroup>
        </FormGroup>
      </Col>
    </Row>
  );
};

const LoginForms = () => {
  const location = useLocation();
  const [state, setState] = useState({
    email: "",
    password: "",
    confirmPassword: "",
    code: "",
  });
  const onChange = (field) => (e) => setState(R.assoc(field, e.target.value));
  const onLogin = useLogin(state);
  const onRegister = useRegister(state);
  const onForgotPassword = useForgotPassword(state);
  const onCodeLogin = useCodeLogin(state);
  const secret = R.path(["state", "secret"], location);
  const onEnableTwoFactor = useEnableTwoFactorLogin({ ...state, secret });
  const twoFactorId = R.path(["state", "twoFactorId"], location);
  const onTwoFactorLogin = useTwoFactorLogin({ ...state, twoFactorId });
  return (
    <div>
      <Row>
        <Col>
          <Route
            path={[
              ROUTES.REGISTER,
              ROUTES.LOGIN,
              ROUTES.FORGOT,
              ROUTES.ENABLE_TWO_FACTOR,
            ]}
          >
            <FormGroup>
              <Input
                size="lg"
                type="email"
                name="email"
                placeholder="Email"
                onChange={onChange("email")}
                value={state.email}
              />
            </FormGroup>
          </Route>
          <Route path={[ROUTES.ENABLE_TWO_FACTOR]}>
            <FormGroup>
              <QRCode
                value={R.path(["state", "secretBase32Encoded"], location)}
                size={350}
              />
              <p>Your Code Is:</p>
              <h4>{R.path(["state", "secretBase32Encoded"], location)}</h4>
            </FormGroup>
          </Route>
          <Route
            path={[
              ROUTES.CODE_LOGIN,
              ROUTES.TWO_FACTOR_LOGIN,
              ROUTES.ENABLE_TWO_FACTOR,
            ]}
          >
            <FormGroup>
              <Input
                size="lg"
                name="code"
                placeholder="Code"
                onChange={onChange("code")}
                value={state.code}
              />
            </FormGroup>
          </Route>

          <Route path={[ROUTES.REGISTER, ROUTES.LOGIN]}>
            <FormGroup>
              <Input
                size="lg"
                type="password"
                name="password"
                placeholder="Password"
                value={state.password}
                onChange={onChange("password")}
              />
            </FormGroup>
          </Route>

          <Route path={ROUTES.REGISTER}>
            <FormGroup>
              <Input
                size="lg"
                type="password"
                name="confirmPassword"
                placeholder="Confirm Password"
                onChange={onChange("confirmPassword")}
                value={state.confirmPassword}
              />
            </FormGroup>
          </Route>
        </Col>
      </Row>
      <hr />
      <Row>
        <Col>
          <Route path={ROUTES.LOGIN}>
            <Button size="lg" outline block color="primary" onClick={onLogin}>
              Login
            </Button>
          </Route>
          <Route path={ROUTES.REGISTER}>
            <Button
              size="lg"
              outline
              block
              color="primary"
              onClick={onRegister}
            >
              Register
            </Button>
          </Route>
          <Route path={ROUTES.FORGOT}>
            <Button
              size="lg"
              outline
              block
              color="primary"
              onClick={onForgotPassword}
            >
              Reset Password
            </Button>
          </Route>
          <Route path={ROUTES.CODE_LOGIN}>
            <Button
              size="lg"
              outline
              block
              color="primary"
              onClick={onCodeLogin}
            >
              Login with code
            </Button>
          </Route>
          <Route path={ROUTES.ENABLE_TWO_FACTOR}>
            <Button
              size="lg"
              outline
              block
              color="primary"
              onClick={onEnableTwoFactor}
            >
              Enable Two Factor Login
            </Button>
          </Route>
          <Route path={ROUTES.TWO_FACTOR_LOGIN}>
            <Button
              size="lg"
              outline
              block
              color="primary"
              onClick={onTwoFactorLogin}
            >
              Two Factor Login
            </Button>
          </Route>
          <Route path={[ROUTES.REGISTER, ROUTES.LOGIN]}>
            <Link to={ROUTES.FORGOT}>
              <Button className="mt-1" color="link">
                Forgot your password?
              </Button>
            </Link>
            <hr />
            <LoginWithFacebook />
            <LoginWithOneTimeCode {...state} />
            <EnableTwoFactor />
          </Route>
        </Col>
      </Row>
    </div>
  );
};

const useLogin = ({ email, password }) => {
  const { login } = useAuth();
  return () => login({ email, password });
};

const useRegister = ({ email, password, confirmPassword }) => {
  const { register } = useAuth();
  return () => {
    if (password !== confirmPassword) return;
    register({ email, password });
  };
};

const useForgotPassword = ({ email }) => {
  const { forgotPassword } = useAuth();
  return () => forgotPassword({ email });
};

const useCodeLogin = ({ code }) => {
  const { completePasswordlessLogin } = useAuth();
  return () => completePasswordlessLogin({ code });
};

const useEnableTwoFactorLogin = ({ email, secret, code }) => {
  const { enableTwoFactor } = useAuth();
  return () => enableTwoFactor({ email, secret, code });
};

const useTwoFactorLogin = ({ code, twoFactorId }) => {
  const { completeTwoFactorAuthentication } = useAuth();
  return () => completeTwoFactorAuthentication({ code, twoFactorId });
};

const LoginWithFacebook = () => {
  const { loginWithFacebook } = useAuth();
  const handleFacebookLogin = ({ accessToken }) => {
    console.log(accessToken);
    if (accessToken) loginWithFacebook({ accessToken });
  };
  return (
    <FacebookLogin
      appId={window["CONCEPTS_CONFIG"].REACT_APP_FACEBOOK_APP_ID}
      fields="email"
      callback={handleFacebookLogin}
      size="medium"
      version="5.0"
      render={(renderProps) => {
        return <FacebookLoginButton onClick={renderProps.onClick} />;
      }}
    />
  );
};

const LoginWithOneTimeCode = ({ email }) => {
  const { startPasswordlessLogin } = useAuth();
  const handlePasswordlessLogin = () => {
    startPasswordlessLogin({ email });
  };

  return <OneTimeLoginButton onClick={handlePasswordlessLogin} />;
};

const OneTimeLoginButton = createButton({
  text: "Login with a One-Time Code",
  icon: () => <FontAwesomeIcon icon={faEnvelope} size="lg" />,
  style: { background: "#666" },
  activeStyle: { background: "#444" },
});

const EnableTwoFactor = () => {
  const { generateTwoFactorSecret } = useAuth();
  const handleTwoFactorLogin = () => {
    generateTwoFactorSecret();
  };
  return (
    <GoogleLoginButton onClick={handleTwoFactorLogin}>
      <span>Enable Two Factor</span>
    </GoogleLoginButton>
  );
};
