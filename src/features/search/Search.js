import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "reactstrap";
import { useSearch } from "./Provider";
import { Link } from "react-router-dom";
import InfiniteScroll from "react-infinite-scroller";
import { Node, mapNodeToView } from "../nodes/Node";
import { ROUTES } from "../../Router";
import { useSelector } from "../../auth";
import selectors from "productive-auth/selectors";
import { inc } from "ramda";

const Search = () => {
  const [prevDataSize, setDataSize] = useState(0);
  const [hasDataChanged, setHasDataChanged] = useState(0);
  const { loading, error, data, nextPage } = useSearch();

  const dataSize = data?.length || 0;
  useEffect(() => {
    if (prevDataSize !== dataSize) {
      setDataSize(dataSize);
      setHasDataChanged(0);
    } else {
      setHasDataChanged(inc);
    }
  }, [data, dataSize, prevDataSize]);

  if (loading && !data) return null;
  if (error) return `Error! ${error}`;

  return (
    <Container className="py-3">
      <InfiniteScroll
        pageStart={0}
        loadMore={nextPage}
        hasMore={hasDataChanged < 1}
        loader={
          <div className="loader" key={0}>
            Loading ...
          </div>
        }
      >
        {data.map((node, i) => (
          <Row key={node.id}>
            <Col
              className={`d-flex justify-content-${
                i % 2 === 0 ? "start" : "end"
              }`}
            >
              <Link to={`${ROUTES.NODE_PAGE}/${node.id}`}>
                <Node node={mapNodeToView(node)} />
              </Link>
            </Col>
          </Row>
        ))}
      </InfiniteScroll>
    </Container>
  );
};

export default () => {
  const startup = useSelector(selectors.startup);
  if (startup) return "Trying to Login";
  return <Search />;
};
