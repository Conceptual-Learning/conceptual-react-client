import React from "react";
import { useApolloClient } from "@apollo/react-hooks";
import { useHistory } from "react-router-dom";
import gql from "graphql-tag";
import { createFeedContext, createApolloHook } from "productive-feed";
import { createHistoryHook } from "productive-feed";
import { useSelector } from "../../auth";
import selectors from "productive-auth/selectors";

const [FeedProvider, useFeed] = createFeedContext();

const SEARCH_CONCEPTS = gql`
  query Concepts($limit: Int, $offset: Int, $query: String!) {
    data: concepts(
      limit: $limit
      offset: $offset
      order_by: { id: desc }
      where: { name: { _ilike: $query } }
    ) {
      id
      name
    }
  }
`;

export const SearchProvider = ({ children }) => {
  const startup = useSelector(selectors.startup);
  return startup ? (
    children
  ) : (
    <FeedProvider
      useFetch={createApolloHook(useApolloClient, SEARCH_CONCEPTS)}
      useFilters={createHistoryHook(useHistory)}
      options={{ defaults: { pageSize: 50 } }}
    >
      {children}
    </FeedProvider>
  );
};

export const useSearch = useFeed;
