import React from "react";
import { Button } from "reactstrap";
import { Container, Row, Col } from "reactstrap";
import { Form, FormGroup, Input, Label } from "reactstrap";
import gql from "graphql-tag";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { useHistory } from "react-router-dom";
import { ROUTES } from "../../Router";

const GET_CONCEPTS = gql`
  query GetConcepts {
    concepts(order_by: { name: asc }) {
      id
      name
    }
  }
`;

const CREATE_RESOURCE = gql`
  mutation CreateReference($resource: concepts_resources_insert_input!) {
    resource: insert_concepts_resources_one(object: $resource) {
      id
    }
  }
`;

const NodeOptions = () => {
  const { loading, error, data } = useQuery(GET_CONCEPTS);
  if (loading) return null;
  if (error) return `Error! ${error}`;
  const { concepts } = data;
  return concepts.map((c) => (
    <option key={c.id} value={c.id}>
      {c.name}
    </option>
  ));
};

export default () => {
  const [createResource] = useMutation(CREATE_RESOURCE);
  const history = useHistory();
  return (
    <Container>
      <br />
      <Row>
        <Col>
          <h1 className="text-center">Add Reference</h1>
        </Col>
      </Row>
      <br />
      <Form
        onSubmit={(e) => {
          e.preventDefault();
          const concept_id = e.target.conceptId.value;
          const url = e.target.url.value;
          createResource({
            variables: {
              resource: {
                url,
                concepts_tags: {
                  data: {
                    concept_id,
                  },
                },
              },
            },
          }).then(() => {
            history.push(`${ROUTES.NODE_PAGE}/${concept_id}`);
          });
        }}
      >
        <Row>
          <Col>
            <FormGroup>
              <Label for="concept">Node:</Label>
              <Input type="select" name="conceptId">
                <NodeOptions />
              </Input>
            </FormGroup>
            <FormGroup>
              <Label for="url">Reference:</Label>
              <Input size="lg" name="url" placeholder="Type an URL" />
            </FormGroup>
          </Col>
        </Row>
        <hr />
        <Row>
          <Col className="text-center">
            <Button outline color="primary" size="lg">
              Add Reference
            </Button>
          </Col>
        </Row>
      </Form>
    </Container>
  );
};
