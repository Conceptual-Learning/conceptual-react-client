import React from "react";
import { Button } from "reactstrap";
import { Container, Row, Col } from "reactstrap";
import { Form, FormGroup, Input, Label } from "reactstrap";
import gql from "graphql-tag";
import { useMutation } from "@apollo/react-hooks";
import { useHistory } from "react-router-dom";
import { ROUTES } from "../../Router";

const CREATE_CONCEPT = gql`
  mutation CreateConcept($concept: concepts_concepts_insert_input!) {
    insert_concepts_concepts_one(object: $concept) {
      id
    }
  }
`;

export default () => {
  const history = useHistory();
  const [createConcept] = useMutation(CREATE_CONCEPT);
  return (
    <Container>
      <br />
      <Row>
        <Col>
          <h1 className="text-center">New Concept</h1>
        </Col>
      </Row>
      <br />

      <Form
        onSubmit={(e) => {
          e.preventDefault();
          const name = e.target.name.value;
          createConcept({
            variables: {
              concept: { name },
            },
          }).then(({ data }) => {
            const id = data.insert_concepts_concepts_one.id;
            history.push(`${ROUTES.NODE_PAGE}/${id}`);
          });
        }}
      >
        <Row>
          <Col>
            <FormGroup>
              <Label for="name">Concept Name:</Label>
              <Input size="lg" name="name" placeholder="Type a name" />
            </FormGroup>
          </Col>
        </Row>
        <hr />
        <Row>
          <Col className="text-center">
            <Button outline color="primary" size="lg">
              Create Concept
            </Button>
          </Col>
        </Row>
      </Form>
    </Container>
  );
};
