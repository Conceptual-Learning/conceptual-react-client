import React from "react";
import { Button } from "reactstrap";
import { Container, Row, Col } from "reactstrap";
import { Form, FormGroup, Input, Label } from "reactstrap";
import gql from "graphql-tag";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { useHistory } from "react-router-dom";
import { ROUTES } from "../../Router";

const GET_CONCEPTS = gql`
  query GetConcepts {
    concepts(order_by: { name: asc }) {
      id
      name
    }
  }
`;

const NodeOptions = () => {
  const { loading, error, data } = useQuery(GET_CONCEPTS);
  if (loading) return null;
  if (error) return `Error! ${error}`;
  const { concepts } = data;
  return concepts.map((c) => (
    <option key={c.id} value={c.id}>
      {c.name}
    </option>
  ));
};

const CREATE_RELATION = gql`
  mutation CreateRelation($from: Int!, $to: Int!, $description: String!) {
    insert_concepts_relations_one(
      object: {
        child_concept_id: $from
        parent_concept_id: $to
        description: $description
      }
    ) {
      child_concept_id
    }
  }
`;

export default () => {
  const history = useHistory();
  const [createRelation] = useMutation(CREATE_RELATION);
  return (
    <Container>
      <br />
      <Row>
        <Col>
          <h1 className="text-center">Add Relation</h1>
        </Col>
      </Row>
      <br />
      <Form
        onSubmit={(e) => {
          e.preventDefault();
          const from = e.target.from.value;
          const to = e.target.to.value;
          const description = e.target.description.value;
          if (from !== to)
            createRelation({ variables: { from, to, description } }).then(
              ({ data }) => {
                const id = data.insert_concepts_relations_one.child_concept_id;
                history.push(`${ROUTES.NODE_PAGE}/${id}`);
              }
            );
        }}
      >
        <Row>
          <Col>
            <FormGroup>
              <Label for="from">From:</Label>
              <Input type="select" name="from">
                <NodeOptions />
              </Input>
            </FormGroup>
            <FormGroup>
              <Label for="to">To:</Label>
              <Input type="select" name="to">
                <NodeOptions />
              </Input>
            </FormGroup>
            <FormGroup>
              <Label for="description">Description:</Label>
              <Input
                size="lg"
                name="description"
                placeholder="Describe the relation"
              />
            </FormGroup>
          </Col>
        </Row>
        <hr />
        <Row>
          <Col className="text-center">
            <Button outline color="primary" size="lg">
              Add Relation
            </Button>
          </Col>
        </Row>
      </Form>
    </Container>
  );
};
