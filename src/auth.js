import { createFusionAuthClient } from "productive-auth/clients/FusionAuth";
import { createService } from "productive-auth";
import { createLocalForage } from "productive-auth/storage-providers/LocalForage";
export * from "productive-auth/providers/ReactProvider";

const client = createFusionAuthClient({
  endpoint: window["CONCEPTS_CONFIG"].REACT_APP_FUSION_AUTH_ENDPOINT,
  applicationId: window["CONCEPTS_CONFIG"].REACT_APP_FUSION_AUTH_APP_ID,
  apiKey: window["CONCEPTS_CONFIG"].REACT_APP_FUSION_AUTH_API_KEY,
});

export const authService = createService({
  client,
  storage: createLocalForage("concepts"),
});
